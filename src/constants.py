"""Script for constants used in this repository"""
import os

DATA_DIR = "./data"

APOPTOSIS_DRUGS = [
    "Embelin",
    "Obatoclax Mesylate",
    "PAC-1",
    "TW 37",
    "XMD13-2",
    "rTRAIL",
]
