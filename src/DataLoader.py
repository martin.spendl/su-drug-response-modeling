"""DataLoader class that Lazy loads data"""
from src.constants import DATA_DIR, APOPTOSIS_DRUGS
import pandas as pd
from sklearn.preprocessing import StandardScaler


class DataLoader:
    def __init__(self):

        self.cell_line_metadata = None
        self.cell_line_exp = None
        self.cell_line_response = None

    def get_expressions(self) -> pd.DataFrame:

        if self.cell_line_exp is not None:
            return self.cell_line_exp.copy()

        self.cell_line_exp = pd.read_csv(
            f"{DATA_DIR}/cell_line_expressions.csv", sep="\t", index_col=["Samples"]
        )

        return self.cell_line_exp.copy()

    def get_drug_response(self) -> pd.DataFrame:

        if self.cell_line_response is not None:
            return self.cell_line_response.copy()

        self.cell_line_response = pd.read_csv(
            f"{DATA_DIR}/raw_GDSC_data/drug_response.csv",
            sep="\t",
            index_col=["Screened Compounds:"],
        )
        self.cell_line_response.index.name = "Cell Lines"

        return self.cell_line_response.copy()

    def get_metadata(self) -> pd.DataFrame:

        if self.cell_line_metadata is not None:
            return self.cell_line_metadata.copy()

        self.cell_line_metadata = pd.read_csv(
            f"{DATA_DIR}/raw_GDSC_data/cell_line_metadata.csv",
            sep="\t",
            index_col=["Sample Name"],
        )

        return self.cell_line_metadata.copy()

    def get_exp_and_response_for_a_drug(self, drug_name: str = "Erlotinib") -> tuple:

        exp = self.get_expressions()
        response = self.get_drug_response()

        if drug_name not in response.columns.values:
            raise KeyError(f"Not a valid drug name: {drug_name}.")

        drug_response = response[response[drug_name].isin(["R", "S"])][drug_name]
        common_cl = sorted(
            list(set(drug_response.index.values).intersection(set(exp.index.values)))
        )

        drug_response = drug_response.loc[common_cl].to_frame()
        drug_response[f"{drug_name}_sensitive"] = drug_response.apply(
            lambda row: row[drug_name] == "S", axis=1
        )
        cl_exp = exp.loc[common_cl]

        return cl_exp, drug_response[f"{drug_name}_sensitive"].astype(int)

    def get_apoptotic_drug_dataset_split(self, N_samples_test: float = 0.1):

        drug_exp_and_responses = {
            drug: self.get_exp_and_response_for_a_drug(drug) for drug in APOPTOSIS_DRUGS
        }

        common_samples = pd.concat(
            [drug_exp_and_responses[drug][1] for drug in APOPTOSIS_DRUGS],
            axis=1,
            join="inner",
        )

        test_samples = common_samples.sample(
            random_state=42, frac=N_samples_test, replace=False
        ).index.values

        return {
            drug: self.construct_train_test_set(
                drug_exp_and_responses[drug], test_samples
            )
            for drug in APOPTOSIS_DRUGS
        }

    def get_L1000_genes(self):

        return pd.read_csv(f"{DATA_DIR}/L1000_genes_cut.csv", sep="\t")[
            "pr_gene_symbol"
        ].values

    def construct_train_test_set(self, exp_and_response, test_samples):

        exp, response = exp_and_response

        # full gene list
        train_X = exp.loc[~exp.index.isin(test_samples)]
        test_X = exp.loc[test_samples]
        scaler = StandardScaler()
        train_X = pd.DataFrame(
            scaler.fit_transform(train_X), index=train_X.index, columns=train_X.columns
        )
        test_X = pd.DataFrame(
            scaler.transform(test_X), index=test_X.index, columns=test_X.columns
        )

        # L1000
        L1000_GENES = self.get_L1000_genes()
        exp = exp[L1000_GENES]

        train_X_l1000 = exp.loc[~exp.index.isin(test_samples)]
        test_X_l1000 = exp.loc[test_samples]
        scaler = StandardScaler()
        train_X_l1000 = pd.DataFrame(
            scaler.fit_transform(train_X_l1000),
            index=train_X_l1000.index,
            columns=train_X_l1000.columns,
        )
        test_X_l1000 = pd.DataFrame(
            scaler.transform(test_X_l1000),
            index=test_X_l1000.index,
            columns=test_X_l1000.columns,
        )

        return {
            "train-std": [train_X, response.loc[train_X.index.values]],
            "test-std": [test_X, response.loc[test_X.index.values]],
            "train-std-l1000": [
                train_X_l1000,
                response.loc[train_X_l1000.index.values],
            ],
            "test-std-l1000": [test_X_l1000, response.loc[test_X_l1000.index.values]],
        }


if __name__ == "__main__":

    dataloader = DataLoader()

    d = dataloader.get_apoptotic_drug_dataset_split()
