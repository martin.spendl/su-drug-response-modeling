"""Script with metric implementation"""
from sklearn import metrics


def pred_to_int(y_pred):
    return [0 if prediction < 0.5 else 1 for prediction in y_pred]


def metric_accuracy(y_true, y_pred):
    y_pred_int = pred_to_int(y_pred)
    return metrics.accuracy_score(y_true, y_pred_int)


def precision(y_true, y_pred):
    y_pred_int = pred_to_int(y_pred)
    return metrics.precision_score(y_true, y_pred_int)


def recall(y_true, y_pred):
    y_pred_int = pred_to_int(y_pred)
    return metrics.recall_score(y_true, y_pred_int)


def F1_score(y_true, y_pred):
    y_pred_int = pred_to_int(y_pred)
    return metrics.f1_score(y_true, y_pred_int)


METRICS = {
    "ROC_AUC": metrics.roc_auc_score,
    "precision": precision,
    "recall": recall,
    "F1": F1_score,
    "brier_score_loss": metrics.brier_score_loss,
    "accuracy": metric_accuracy,
}
