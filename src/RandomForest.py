"""RandomForest Class"""
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import GridSearchCV

from src.AbstractModel import AbstractModel


class RandomForestModel(AbstractModel):
    def __init__(self, model_name: str = "RandomForest") -> None:
        super().__init__(model_name)

        if self.model_exists():
            self.load_model()

    def train_with_grid_search(
        self, train_X, train_y, scoring="neg_mean_squared_error", CV=10, retrain=False
    ):

        if self.model is not None and not retrain:
            print("This model was already trained.")
            return None

        self.model = RandomForestClassifier(
            min_samples_split=10, class_weight="balanced"
        )

        grid_search_dict = {
            "n_estimators": [10, 50, 100],
            "criterion": ["gini", "entropy"],
        }

        self.grid = GridSearchCV(
            self.model, param_grid=grid_search_dict, scoring=scoring, cv=CV, n_jobs=-1
        )

        self.grid.fit(train_X, train_y)

        self.model = self.grid.best_estimator_

        self.save_model()

    def predict_proba(self, test_X):
        return self.model.predict_proba(test_X)
