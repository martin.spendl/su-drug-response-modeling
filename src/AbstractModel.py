"""AbstracModel class with useful functions"""
from abc import abstractmethod
import os
import numpy as np
import pickle
import pandas as pd

from src.constants import DATA_DIR
from src.metrics import METRICS


class AbstractModel:
    def __init__(self, model_name: str) -> None:

        self.model_name = model_name

        self.create_model_folder()

        self.model = None

    def create_model_folder(self):

        if self.model_name in os.listdir(DATA_DIR):
            return None

        os.mkdir(f"{DATA_DIR}/{self.model_name}")

    def model_exists(self):
        return f"{self.model_name}_model.pickle" in os.listdir(
            f"{DATA_DIR}/{self.model_name}"
        )

    def load_model(self):

        with open(
            f"{DATA_DIR}/{self.model_name}/{self.model_name}_model.pickle", "rb"
        ) as f:
            self.model = pickle.load(f)

    def save_model(self):

        with open(
            f"{DATA_DIR}/{self.model_name}/{self.model_name}_model.pickle", "wb"
        ) as f:
            pickle.dump(self.model, f)

    @abstractmethod
    def predict_proba(self):
        raise NotImplemented("implement predict_proba()")

    def evaluate(self, test_X, test_y):

        probas = self.predict_proba(test_X)[:, 1]

        return pd.Series(
            {
                metric: metric_func(test_y, probas)
                for metric, metric_func in METRICS.items()
            },
            name=self.model_name,
        )
