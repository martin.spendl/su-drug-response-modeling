"""LosicticRegression Class"""
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import GridSearchCV

from src.AbstractModel import AbstractModel


class LogisticRegressionModel(AbstractModel):
    def __init__(self, model_name: str = "LogisticRegression") -> None:
        super().__init__(model_name)

        if self.model_exists():
            self.load_model()

    def train_with_grid_search(
        self, train_X, train_y, scoring="neg_mean_squared_error", CV=10, retrain=False
    ):

        if self.model is not None and not retrain:
            print("This model was already trained.")
            return None

        self.model = LogisticRegression(
            penalty="elasticnet",
            class_weight="balanced",
            solver="saga",
            max_iter=100000,
        )

        grid_search_dict = {
            "C": [0.1],  # [0.001, 0.1, 1, 2, 5, 10, 100],
            "l1_ratio": [0.5],  # [0, 0.25, 0.5, 0.75, 1],
        }

        self.grid = GridSearchCV(
            self.model, param_grid=grid_search_dict, scoring=scoring, cv=CV, n_jobs=-1
        )

        self.grid.fit(train_X, train_y)

        self.model = self.grid.best_estimator_

        self.save_model()

    def predict_proba(self, test_X):
        return self.model.predict_proba(test_X)
