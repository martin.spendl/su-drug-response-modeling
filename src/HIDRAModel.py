"""HIDRAModel Class implemented from https://pubs.acs.org/doi/10.1021/acs.jcim.1c00706"""
import os
import pickle
import pandas as pd

from src.metrics import METRICS
from src.constants import DATA_DIR

import tensorflow as tf
from tensorflow.keras import layers, models
from sklearn.model_selection import train_test_split

from src.AbstractModel import AbstractModel


class HIDRAModel(AbstractModel):
    def __init__(self, model_name: str = "HIDRA") -> None:
        super().__init__(model_name)

        self.pathways = self.read_pathways()

        if self.model_exists():
            self.load_model()

    def read_pathways(self):

        with open(f"{DATA_DIR}/HIDRA_KEGG_pathways.pickle", "rb") as f:
            pathways = pickle.load(f)

        return pathways

    def convert_dataset_into_tf_Dataset_with_pathways(self, X, y) -> tuple:

        dataset_pathways = {
            f"{p}_input": tf.convert_to_tensor(
                [[vals] for vals in X[genes].values], dtype=tf.float32
            )
            for p, genes in self.pathways.items()
        }
        dataset_pathways_y = {
            "response": tf.convert_to_tensor(
                [[1, 0] if i == 0 else [0, 1] for i in y.values], dtype=tf.int32
            )
        }

        return tf.data.Dataset.from_tensor_slices(
            (dataset_pathways, dataset_pathways_y)
        )

    def build_a_model(self):

        input_layers = []

        embedded_layers = []

        for i, (pathway, genes) in enumerate(self.pathways.items()):

            pathway_input = layers.Input(
                shape=(1, len(genes)), dtype="float32", name=pathway + "_input"
            )
            input_layers.append(pathway_input)
            pathway_input = layers.Flatten()(pathway_input)

            # gene-level attention
            pathway_attention = layers.Dense(
                len(genes),
                name=pathway + "_gene_Attention",
                activation="softmax",
            )(pathway_input)
            pathway_attention = layers.Flatten()(pathway_attention)

            pathway_dot = layers.dot(
                [pathway_input, pathway_attention], axes=1, name=pathway + "_dot"
            )

            pathway_batch_norm = layers.BatchNormalization(
                name=pathway + "_batch_norm"
            )(pathway_dot)

            pathway_activation = layers.Activation("relu", name=pathway + "_relu")(
                pathway_batch_norm
            )
            pathway_activation = layers.Flatten()(pathway_activation)

            embedded_layers.append(pathway_activation)

        embedded_concat = layers.Concatenate(axis=-1, name="embedding_concat")(
            embedded_layers
        )

        # pathway-level_attention
        sample_attention = layers.Dense(
            len(embedded_layers), activation="tanh", name="Sample_Attention"
        )(embedded_concat)

        sample_activation = layers.Activation("softmax", name="Sample_Softmax")(
            sample_attention
        )

        sample_multiplied = layers.multiply(
            [embedded_concat, sample_activation], name="Sample_Multiply"
        )
        sample_batch_norm = layers.BatchNormalization(name="Sample_BatchNorm")(
            sample_multiplied
        )
        sample_activation_2 = layers.Activation("relu", name="Sample_relu")(
            sample_batch_norm
        )

        # response prediction
        final_dense = layers.Dense(128, name="Final_dense")(sample_activation_2)
        final_dense = layers.BatchNormalization(name="Final_BatchNorm")(final_dense)
        final_dense = layers.Activation("relu", name="Final_relu")(final_dense)

        output = layers.Dense(2, activation="sigmoid", name="response")(final_dense)

        model = models.Model(inputs=input_layers, outputs=output, name="HIDRA")

        return model

    def train_model(
        self,
        train_X,
        train_y,
        retrain=False,
        loss="binary_crossentropy",
        optimizer="adam",
        EPOCHS=1000,
        rnd_seed=42,
    ):

        if self.model is not None and not retrain:
            print("This model was already trained.")
            return None

        self.model = self.build_a_model()

        self.model.compile(loss=loss, optimizer=optimizer, metrics=["mae"])

        X_train, X_test, y_train, y_test = train_test_split(
            train_X, train_y, random_state=rnd_seed, test_size=0.2
        )

        X = self.convert_dataset_into_tf_Dataset_with_pathways(X_train, y_train)
        X = X.batch(len(X_train))

        X_val = self.convert_dataset_into_tf_Dataset_with_pathways(X_test, y_test)
        X_val = X_val.batch(len(X_test))

        print(f"Training size: {len(X_train)}\nValidation size: {len(X_test)}")

        self.model.summary()

        self.history = self.model.fit(
            X,
            epochs=EPOCHS,
            validation_data=X_val,
            verbose=2,
            callbacks=[tf.keras.callbacks.EarlyStopping(patience=10)],
        )

        self.save_model()

    def predict_proba(self, test_X, test_y):
        X = self.convert_dataset_into_tf_Dataset_with_pathways(test_X, test_y)
        X = X.batch(len(test_X))
        return self.model.predict(X)

    def save_model(self):
        self.model.save(f"{DATA_DIR}/{self.model_name}/{self.model_name}_model.hdf5")

    def load_model(self):
        self.model = models.load_model(
            f"{DATA_DIR}/{self.model_name}/{self.model_name}_model.hdf5"
        )

    def model_exists(self):
        return f"{self.model_name}_model.hdf5" in os.listdir(
            f"{DATA_DIR}/{self.model_name}"
        )

    def evaluate(self, test_X, test_y):

        probas = self.predict_proba(test_X, test_y)[:, 1]

        return pd.Series(
            {
                metric: metric_func(test_y, probas)
                for metric, metric_func in METRICS.items()
            },
            name=self.model_name,
        )
