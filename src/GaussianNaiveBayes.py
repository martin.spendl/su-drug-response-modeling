"""GaussianNaiveBayes Class"""
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import GridSearchCV

from src.AbstractModel import AbstractModel


class GaussianNaiveBayesModel(AbstractModel):
    def __init__(self, model_name: str = "GaussianNaiveBayes") -> None:
        super().__init__(model_name)

        if self.model_exists():
            self.load_model()

    def train_with_grid_search(
        self, train_X, train_y, scoring="neg_mean_squared_error", CV=10, retrain=False
    ):

        if self.model is not None and not retrain:
            print("This model was already trained.")
            return None

        self.model = GaussianNB(priors=[0, 1])

        grid_search_dict = {"var_smoothing": [1e-10, 1e-9, 1e-8, 1e-7]}

        self.grid = GridSearchCV(
            self.model, param_grid=grid_search_dict, scoring=scoring, cv=CV, n_jobs=-1
        )

        self.grid.fit(train_X, train_y)

        self.model = self.grid.best_estimator_

        self.save_model()

    def predict_proba(self, test_X):
        return self.model.predict_proba(test_X)
